﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ModelTest.Data
{
    [DataContract(IsReference = true)]
    public class ImageEntity
    {
        [DataMember, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DataMember]
        public byte[] Entity { get; set; }

        [DataMember]
        public virtual ICollection<TextEntity> TextEntities { get; set; }

    }
}
