﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace ModelTest.Data
{
    [DataContract]
    public class TextEntity
    {
        [DataMember, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DataMember]
        public string Entity { get; set; }

        [DataMember]
        public virtual ICollection<ImageEntity> ImageEntities { get; set; }
    }
}
