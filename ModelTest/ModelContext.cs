﻿using System;
using System.Data.Entity;
using ModelTest.Data;
using ModelTest.Migrations;

namespace ModelTest
{
    public class ModelContext : DbContext
    {

        public ModelContext() : base("name=testDB")
        {
            Database.CommandTimeout = 180;
            // Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ModelContext>()); 
            // DropCreateDatabaseAlways<ModelContext>());
            // Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
        }

        public ModelContext(bool proxyCreationEnabled = true) : this()
        {
            Configuration.ProxyCreationEnabled = proxyCreationEnabled;
        }

        public DbSet<TextEntity> TextEntities { get; set; }
        public DbSet<ImageEntity> ImageEntities { get; set; }
    }
}
