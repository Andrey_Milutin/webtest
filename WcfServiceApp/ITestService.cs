﻿using System.ServiceModel;
using System.Web.Services;
using ModelTest.Data;

namespace WcfServiceApp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITestService" in both code and config file together.
    [ServiceContract]
    public interface ITestService
    {
        [OperationContract]
        void LoadImage(ImageEntity newImage);

        [OperationContract]
        void TextLoad(TextEntity text);

        [OperationContract]
        ImageEntity[] UpLoadImage(int id);

        [OperationContract]
        TextEntity[] UpLoadText();

        [OperationContract]
        TextEntity[] GetText();
    }
}
