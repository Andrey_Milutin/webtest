﻿using ModelTest.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Services;
using ModelTest;

namespace WcfServiceApp
{
    [WebService(Namespace = "https://localhost/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class TestService : ITestService
    {
        public void LoadImage(ImageEntity newImage)
        {
            using (var dbContext = new ModelContext(false))
            {
                dbContext.ImageEntities.Add(newImage);
                dbContext.SaveChanges();
            }
        }

        public ImageEntity[] UpLoadImage(int id)
        {
            using (var dbContext = new ModelContext(false))
            {
                return dbContext.ImageEntities.Where(i => i.Id > id).Take(10).ToArray();
            }
        }

        public void TextLoad(TextEntity text)
        {
            using (var dbContext = new ModelContext(false))
            {
                dbContext.TextEntities.Add(text);
            }
        }

        public TextEntity[] GetText()
        {
            using (var dbContext = new ModelContext(false))
            {
                return dbContext.TextEntities.Select(p => p).ToArray();
            }
        }

        public TextEntity[] UpLoadText()
        {
            using (var dbContext = new ModelContext(false))
            {
                return dbContext.TextEntities.Include(w => w.ImageEntities).ToArray();
            }
        }

    }
}
