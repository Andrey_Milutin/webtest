﻿using System.ServiceModel;
using ModelTest.Data;

namespace TestSolution.ServiceContracts
{
    //[ServiceContract(Namespace = "http://", ProtectionLevel = ProtectionLevel.None, SessionMode = SessionMode.Required)]

    [ServiceContract]
    public interface ITestService
    {
        [OperationContract]
        void LoadImage(ImageEntity newImage);

        [OperationContract]
        void TextLoad(TextEntity text);

        [OperationContract]
        ImageEntity[] UpLoadImage(int id);

        [OperationContract]
        TextEntity[] UpLoadText();
    }
}
