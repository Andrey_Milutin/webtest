﻿using System.Data.Entity;
using System.Linq;
using ModelTest;
using ModelTest.Data;
using TestSolution.ServiceContracts;

namespace TestSolution
{
    //[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class TestService : ITestService
    {
        public void LoadImage(ImageEntity newImage)
        {
            using (var dbContext = new ModelContext(false))
            {
                dbContext.ImageEntities.Add(newImage);
                dbContext.SaveChanges();
            }
        }

        public ImageEntity[] UpLoadImage(int id)
        {
            using (var dbContext = new ModelContext(false))
            {
                return dbContext.ImageEntities.Where(i => i.Id > id).Take(10).ToArray();
            }
        }

        public void TextLoad(TextEntity text)
        {
            using (var dbContext = new ModelContext(false))
            {
                dbContext.TextEntities.Add(text);
            }
        }

        public TextEntity[] UpLoadText()
        {
            using (var dbContext = new ModelContext(false))
            {
                return dbContext.TextEntities.ToArray();
            }
        }

        // PresentationCore.dll

        //private ConvertImageToByteArrray(BitmapImage image)
        //{
        //    var encoder = new PngBitmapEncoder();
        //    encoder.Frames.Add(BitmapFrame.Create(image));
        //    using (var ms = new MemoryStream())
        //    {
        //        encoder.Save(ms);
        //        // в массив
        //        var byteImage = ms.ToArray();
        //    }
        //}

        //private BitmapImage ConvertByteArrayToImage(byte[] byteImage)
        //{
        //    var resultBitmapImage = new BitmapImage();
        //        var strmImg = new MemoryStream(byteImage);
        //        resultBitmapImage.BeginInit();
        //        resultBitmapImage.StreamSource = strmImg;
        //        resultBitmapImage.DecodePixelWidth = 50;
        //        resultBitmapImage.EndInit();
        //        return resultBitmapImage;
        //}
    }
}
