﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebGrease.Css.Extensions;
using WebProject.TestService;

namespace WebProject.Controllers
{
    public class cardDate
    {
        public string TextGood { get; set; }
        public string ImageBase64 { get; set; }
    }
    public class HomeController : Controller
    {
        protected TestServiceClient TestServiceClient = new TestServiceClient();
        public ActionResult Index()
        {
            //TestServiceClient.l;
            return View();
        }
        [HttpGet]
        public JsonResult LoadGrid()
        {
            //ObservableCollection <>
            var data = TestServiceClient.UpLoadText();
            var a = new List <cardDate>();
            foreach (var d in data)
            {
                if (d.ImageEntities != null)
                {
                    foreach (var t in d.ImageEntities)
                    {
                        if (t != null)
                        {
                            a.Add(new cardDate { TextGood = d?.Entity, ImageBase64 = Convert.ToBase64String(t.Entity) });
                        }
                    }

                }
            }

            return Json(a, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UploadImages(dynamic comment)
        {
            if (Request.Files == null || Request.Files.Count < 1)
            {
                throw new Exception("Выберите файл для загрузки");
            }

            try
            {
                //сохраняем на диск
                HttpPostedFileBase upload = Request.Files[0];
                string filename = Guid.NewGuid().ToString() + Path.GetExtension(upload.FileName);
                string path = Path.Combine(Server.MapPath("~/Files"), filename);
                upload.SaveAs(path);
                byte[] fileData = null;
                var test = TestServiceClient.GetText();
                using (var binaryReader = new BinaryReader(upload.InputStream))
                {
                    TestServiceClient.LoadImage(new ImageEntity {Id = 1, TextEntities = test, Entity = binaryReader.ReadBytes(Request.Files[0].ContentLength) }); 
                }

                // Copy the byte array into a string.
                //for (int Loop1 = 0; Loop1 < FileLen; Loop1++)
                //    MyString = MyString + input[Loop1].ToString();                ////сохраняем в БД
                //List<Entities.File> files = new List<Entities.File>() {item};
                //var result = statementBusinessLogic.File_Save(files);
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Ошибка");
            }
        }
    }
}