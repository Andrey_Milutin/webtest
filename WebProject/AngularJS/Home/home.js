﻿(function () {
    'use strict';

    angular
        .module('home', ['ngTouch', 'ui.grid', 'ui.grid.infiniteScroll'])
        .component('fileUploader', fileUploader);

    angular
        .module('common.view', ['home']);
})();