﻿(function () {
    'use strict';

    angular
          .module('home')
          .controller('homeCtrl', ['$rootScope', '$parse', '$scope', '$http', '$timeout', homeCtrl]);

    function homeCtrl($rootScope, $parse, $scope, $http, $timeout) {
        var self = this;
        console.log(self.data);
        self.filesCount = 0;

        //self.strategy = [{ TextGood: "34523543" }, { TextGood: "34523543" }];
        self.data = [];

        self.firstPage = 2;
        self.lastPage = 2;

        self.getFirstData = function () {
            return $http.get('Home/LoadGrid')
            .then(function (response) {
                console.log(response);
                var newData = self.getPage(response.data, self.lastPage);
                console.log(newData);
                console.log(response.data);
                self.data = self.data.concat(newData);
                console.log(self.data);
            });
        };

        self.getFirstData().then(function () {
            $timeout(function () {
                // timeout needed to allow digest cycle to complete,and grid to finish ingesting the data
                // you need to call resetData once you've loaded your data if you want to enable scroll up,
                // it adjusts the scroll position down one pixel so that we can generate scroll up events
                  self.gridApi.infiniteScroll.resetScroll(self.firstPage > 0, self.lastPage < 4);
            });
        });

        self.homeGrid = {
            infiniteScrollRowsFromEnd: 40,
            infiniteScrollUp: true,
            infiniteScrollDown: true,
            columnDefs: getColumn(),
            rowTemplate: getStatusGridRowTemplate(),
            data: self.data,
            onRegisterApi: function (gridApi) {
                gridApi.infiniteScroll.on.needLoadMoreData($scope, self.getDataDown);
                gridApi.infiniteScroll.on.needLoadMoreDataTop($scope, self.getDataUp);
                self.gridApi = gridApi;
            }
        };

        function getStatusGridRowTemplate() {
            return '<div class="ui-grid-cell" style="cursor: pointer;"' +
                'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader }"' +
                'ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name"' +
                'ui-grid-cell>' +
                '</div>';
        }

        function getColumn() {
            return [
            {
                name: 'TextGood',
                field: 'TextGood',
                displayName: 'Текст',
                cellClass: 'text-center',
                enableColumnMenu: false,
                headerCellClass: 'text-center',
                width: '15%'
            },
                {
                    name: 'image',
                    field: 'ImageBase64',
                    displayName: 'Картинка',
                    cellClass: 'text-center',
                    enableColumnMenu: false,
                    headerCellClass: 'text-center',
                    width: '15%'
                }
            ];
        }



        self.getDataDown = function () {
            return $http.get('Home/LoadGrid')
            .then(function (response) {
                console.log(response);
                self.lastPage++;
                var newData = self.getPage(response.data, self.lastPage);
                self.gridApi.infiniteScroll.saveScrollPercentage();
                self.data = self.data.concat(newData);
                return self.gridApi.infiniteScroll.dataLoaded(self.firstPage > 0, self.lastPage < 4).then(function () { self.checkDataLength('up'); });
            })
            .catch(function (error) {
                return self.gridApi.infiniteScroll.dataLoaded();
            });
        };

        self.getDataUp = function () {
            return $http.get('Home/LoadGrid')
            .then(function (response) {
                console.log(response);
                self.firstPage--;
                var newData = self.getPage(response.data, self.firstPage);
                self.gridApi.infiniteScroll.saveScrollPercentage();
                self.data = newData.concat(self.data);
                return self.gridApi.infiniteScroll.dataLoaded(self.firstPage > 0, self.lastPage < 4).then(function () { self.checkDataLength('down'); });
            })
            .catch(function (error) {
                return self.gridApi.infiniteScroll.dataLoaded();
            });
        };

        self.getPage = function (data, page) {
            console.log(page);
            console.log(data.length);
            var res = [];
            for (var i = 0 ; i < data.length; ++i) {
                res.push(data[i]);
            }
            console.log(res);
            return res;
        };

        self.checkDataLength = function (discardDirection) {
            // work out whether we need to discard a page, if so discard from the direction passed in
            if (self.lastPage - self.firstPage > 3) {
                // we want to remove a page
                self.gridApi.infiniteScroll.saveScrollPercentage();

                if (discardDirection === 'up') {
                    self.data = self.data.slice(100);
                    self.firstPage++;
                    $timeout(function () {
                        // wait for grid to ingest data changes
                        self.gridApi.infiniteScroll.dataRemovedTop(self.firstPage > 0, self.lastPage < 4);
                    });
                } else {
                    self.data = self.data.slice(0, 400);
                    self.lastPage--;
                    $timeout(function () {
                        // wait for grid to ingest data changes
                        self.gridApi.infiniteScroll.dataRemovedBottom(self.firstPage > 0, self.lastPage < 4);
                    });
                }
            }
        };

        self.reset = function () {
            self.firstPage = 2;
            self.lastPage = 2;

            // turn off the infinite scroll handling up and down - hopefully this won't be needed after @swalters scrolling changes
            self.gridApi.infiniteScroll.setScrollDirections(false, false);
            self.data = [];

            self.getFirstData().then(function () {
                $timeout(function () {
                    // timeout needed to allow digest cycle to complete,and grid to finish ingesting the data
                    self.gridApi.infiniteScroll.resetScroll(self.firstPage > 0, self.lastPage < 4);
                });
            });
        };
    }
})();