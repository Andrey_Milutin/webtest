﻿(function () {
    'use strict';

    angular
          .module('app', [
                  'ui.uploader',
                  'ui.bootstrap',
                  'ngSanitize',
                  'common.view'
          ]);
})();