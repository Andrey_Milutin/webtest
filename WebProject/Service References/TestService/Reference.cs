﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebProject.TestService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ImageEntity", Namespace="http://schemas.datacontract.org/2004/07/ModelTest.Data", IsReference=true)]
    [System.SerializableAttribute()]
    public partial class ImageEntity : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private byte[] EntityField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private WebProject.TestService.TextEntity[] TextEntitiesField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public byte[] Entity {
            get {
                return this.EntityField;
            }
            set {
                if ((object.ReferenceEquals(this.EntityField, value) != true)) {
                    this.EntityField = value;
                    this.RaisePropertyChanged("Entity");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public WebProject.TestService.TextEntity[] TextEntities {
            get {
                return this.TextEntitiesField;
            }
            set {
                if ((object.ReferenceEquals(this.TextEntitiesField, value) != true)) {
                    this.TextEntitiesField = value;
                    this.RaisePropertyChanged("TextEntities");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="TextEntity", Namespace="http://schemas.datacontract.org/2004/07/ModelTest.Data")]
    [System.SerializableAttribute()]
    public partial class TextEntity : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string EntityField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private WebProject.TestService.ImageEntity[] ImageEntitiesField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Entity {
            get {
                return this.EntityField;
            }
            set {
                if ((object.ReferenceEquals(this.EntityField, value) != true)) {
                    this.EntityField = value;
                    this.RaisePropertyChanged("Entity");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public WebProject.TestService.ImageEntity[] ImageEntities {
            get {
                return this.ImageEntitiesField;
            }
            set {
                if ((object.ReferenceEquals(this.ImageEntitiesField, value) != true)) {
                    this.ImageEntitiesField = value;
                    this.RaisePropertyChanged("ImageEntities");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="TestService.ITestService")]
    public interface ITestService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITestService/LoadImage", ReplyAction="http://tempuri.org/ITestService/LoadImageResponse")]
        void LoadImage(WebProject.TestService.ImageEntity newImage);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/ITestService/LoadImage", ReplyAction="http://tempuri.org/ITestService/LoadImageResponse")]
        System.IAsyncResult BeginLoadImage(WebProject.TestService.ImageEntity newImage, System.AsyncCallback callback, object asyncState);
        
        void EndLoadImage(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITestService/TextLoad", ReplyAction="http://tempuri.org/ITestService/TextLoadResponse")]
        void TextLoad(WebProject.TestService.TextEntity text);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/ITestService/TextLoad", ReplyAction="http://tempuri.org/ITestService/TextLoadResponse")]
        System.IAsyncResult BeginTextLoad(WebProject.TestService.TextEntity text, System.AsyncCallback callback, object asyncState);
        
        void EndTextLoad(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITestService/UpLoadImage", ReplyAction="http://tempuri.org/ITestService/UpLoadImageResponse")]
        WebProject.TestService.ImageEntity[] UpLoadImage(int id);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/ITestService/UpLoadImage", ReplyAction="http://tempuri.org/ITestService/UpLoadImageResponse")]
        System.IAsyncResult BeginUpLoadImage(int id, System.AsyncCallback callback, object asyncState);
        
        WebProject.TestService.ImageEntity[] EndUpLoadImage(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITestService/UpLoadText", ReplyAction="http://tempuri.org/ITestService/UpLoadTextResponse")]
        WebProject.TestService.TextEntity[] UpLoadText();
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/ITestService/UpLoadText", ReplyAction="http://tempuri.org/ITestService/UpLoadTextResponse")]
        System.IAsyncResult BeginUpLoadText(System.AsyncCallback callback, object asyncState);
        
        WebProject.TestService.TextEntity[] EndUpLoadText(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ITestService/GetText", ReplyAction="http://tempuri.org/ITestService/GetTextResponse")]
        WebProject.TestService.TextEntity[] GetText();
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/ITestService/GetText", ReplyAction="http://tempuri.org/ITestService/GetTextResponse")]
        System.IAsyncResult BeginGetText(System.AsyncCallback callback, object asyncState);
        
        WebProject.TestService.TextEntity[] EndGetText(System.IAsyncResult result);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ITestServiceChannel : WebProject.TestService.ITestService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class UpLoadImageCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        public UpLoadImageCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        public WebProject.TestService.ImageEntity[] Result {
            get {
                base.RaiseExceptionIfNecessary();
                return ((WebProject.TestService.ImageEntity[])(this.results[0]));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class UpLoadTextCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        public UpLoadTextCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        public WebProject.TestService.TextEntity[] Result {
            get {
                base.RaiseExceptionIfNecessary();
                return ((WebProject.TestService.TextEntity[])(this.results[0]));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class GetTextCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        public GetTextCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        public WebProject.TestService.TextEntity[] Result {
            get {
                base.RaiseExceptionIfNecessary();
                return ((WebProject.TestService.TextEntity[])(this.results[0]));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class TestServiceClient : System.ServiceModel.ClientBase<WebProject.TestService.ITestService>, WebProject.TestService.ITestService {
        
        private BeginOperationDelegate onBeginLoadImageDelegate;
        
        private EndOperationDelegate onEndLoadImageDelegate;
        
        private System.Threading.SendOrPostCallback onLoadImageCompletedDelegate;
        
        private BeginOperationDelegate onBeginTextLoadDelegate;
        
        private EndOperationDelegate onEndTextLoadDelegate;
        
        private System.Threading.SendOrPostCallback onTextLoadCompletedDelegate;
        
        private BeginOperationDelegate onBeginUpLoadImageDelegate;
        
        private EndOperationDelegate onEndUpLoadImageDelegate;
        
        private System.Threading.SendOrPostCallback onUpLoadImageCompletedDelegate;
        
        private BeginOperationDelegate onBeginUpLoadTextDelegate;
        
        private EndOperationDelegate onEndUpLoadTextDelegate;
        
        private System.Threading.SendOrPostCallback onUpLoadTextCompletedDelegate;
        
        private BeginOperationDelegate onBeginGetTextDelegate;
        
        private EndOperationDelegate onEndGetTextDelegate;
        
        private System.Threading.SendOrPostCallback onGetTextCompletedDelegate;
        
        public TestServiceClient() {
        }
        
        public TestServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public TestServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TestServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TestServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> LoadImageCompleted;
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> TextLoadCompleted;
        
        public event System.EventHandler<UpLoadImageCompletedEventArgs> UpLoadImageCompleted;
        
        public event System.EventHandler<UpLoadTextCompletedEventArgs> UpLoadTextCompleted;
        
        public event System.EventHandler<GetTextCompletedEventArgs> GetTextCompleted;
        
        public void LoadImage(WebProject.TestService.ImageEntity newImage) {
            base.Channel.LoadImage(newImage);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public System.IAsyncResult BeginLoadImage(WebProject.TestService.ImageEntity newImage, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginLoadImage(newImage, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public void EndLoadImage(System.IAsyncResult result) {
            base.Channel.EndLoadImage(result);
        }
        
        private System.IAsyncResult OnBeginLoadImage(object[] inValues, System.AsyncCallback callback, object asyncState) {
            WebProject.TestService.ImageEntity newImage = ((WebProject.TestService.ImageEntity)(inValues[0]));
            return this.BeginLoadImage(newImage, callback, asyncState);
        }
        
        private object[] OnEndLoadImage(System.IAsyncResult result) {
            this.EndLoadImage(result);
            return null;
        }
        
        private void OnLoadImageCompleted(object state) {
            if ((this.LoadImageCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.LoadImageCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void LoadImageAsync(WebProject.TestService.ImageEntity newImage) {
            this.LoadImageAsync(newImage, null);
        }
        
        public void LoadImageAsync(WebProject.TestService.ImageEntity newImage, object userState) {
            if ((this.onBeginLoadImageDelegate == null)) {
                this.onBeginLoadImageDelegate = new BeginOperationDelegate(this.OnBeginLoadImage);
            }
            if ((this.onEndLoadImageDelegate == null)) {
                this.onEndLoadImageDelegate = new EndOperationDelegate(this.OnEndLoadImage);
            }
            if ((this.onLoadImageCompletedDelegate == null)) {
                this.onLoadImageCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnLoadImageCompleted);
            }
            base.InvokeAsync(this.onBeginLoadImageDelegate, new object[] {
                        newImage}, this.onEndLoadImageDelegate, this.onLoadImageCompletedDelegate, userState);
        }
        
        public void TextLoad(WebProject.TestService.TextEntity text) {
            base.Channel.TextLoad(text);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public System.IAsyncResult BeginTextLoad(WebProject.TestService.TextEntity text, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginTextLoad(text, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public void EndTextLoad(System.IAsyncResult result) {
            base.Channel.EndTextLoad(result);
        }
        
        private System.IAsyncResult OnBeginTextLoad(object[] inValues, System.AsyncCallback callback, object asyncState) {
            WebProject.TestService.TextEntity text = ((WebProject.TestService.TextEntity)(inValues[0]));
            return this.BeginTextLoad(text, callback, asyncState);
        }
        
        private object[] OnEndTextLoad(System.IAsyncResult result) {
            this.EndTextLoad(result);
            return null;
        }
        
        private void OnTextLoadCompleted(object state) {
            if ((this.TextLoadCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.TextLoadCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void TextLoadAsync(WebProject.TestService.TextEntity text) {
            this.TextLoadAsync(text, null);
        }
        
        public void TextLoadAsync(WebProject.TestService.TextEntity text, object userState) {
            if ((this.onBeginTextLoadDelegate == null)) {
                this.onBeginTextLoadDelegate = new BeginOperationDelegate(this.OnBeginTextLoad);
            }
            if ((this.onEndTextLoadDelegate == null)) {
                this.onEndTextLoadDelegate = new EndOperationDelegate(this.OnEndTextLoad);
            }
            if ((this.onTextLoadCompletedDelegate == null)) {
                this.onTextLoadCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnTextLoadCompleted);
            }
            base.InvokeAsync(this.onBeginTextLoadDelegate, new object[] {
                        text}, this.onEndTextLoadDelegate, this.onTextLoadCompletedDelegate, userState);
        }
        
        public WebProject.TestService.ImageEntity[] UpLoadImage(int id) {
            return base.Channel.UpLoadImage(id);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public System.IAsyncResult BeginUpLoadImage(int id, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginUpLoadImage(id, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public WebProject.TestService.ImageEntity[] EndUpLoadImage(System.IAsyncResult result) {
            return base.Channel.EndUpLoadImage(result);
        }
        
        private System.IAsyncResult OnBeginUpLoadImage(object[] inValues, System.AsyncCallback callback, object asyncState) {
            int id = ((int)(inValues[0]));
            return this.BeginUpLoadImage(id, callback, asyncState);
        }
        
        private object[] OnEndUpLoadImage(System.IAsyncResult result) {
            WebProject.TestService.ImageEntity[] retVal = this.EndUpLoadImage(result);
            return new object[] {
                    retVal};
        }
        
        private void OnUpLoadImageCompleted(object state) {
            if ((this.UpLoadImageCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.UpLoadImageCompleted(this, new UpLoadImageCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void UpLoadImageAsync(int id) {
            this.UpLoadImageAsync(id, null);
        }
        
        public void UpLoadImageAsync(int id, object userState) {
            if ((this.onBeginUpLoadImageDelegate == null)) {
                this.onBeginUpLoadImageDelegate = new BeginOperationDelegate(this.OnBeginUpLoadImage);
            }
            if ((this.onEndUpLoadImageDelegate == null)) {
                this.onEndUpLoadImageDelegate = new EndOperationDelegate(this.OnEndUpLoadImage);
            }
            if ((this.onUpLoadImageCompletedDelegate == null)) {
                this.onUpLoadImageCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnUpLoadImageCompleted);
            }
            base.InvokeAsync(this.onBeginUpLoadImageDelegate, new object[] {
                        id}, this.onEndUpLoadImageDelegate, this.onUpLoadImageCompletedDelegate, userState);
        }
        
        public WebProject.TestService.TextEntity[] UpLoadText() {
            return base.Channel.UpLoadText();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public System.IAsyncResult BeginUpLoadText(System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginUpLoadText(callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public WebProject.TestService.TextEntity[] EndUpLoadText(System.IAsyncResult result) {
            return base.Channel.EndUpLoadText(result);
        }
        
        private System.IAsyncResult OnBeginUpLoadText(object[] inValues, System.AsyncCallback callback, object asyncState) {
            return this.BeginUpLoadText(callback, asyncState);
        }
        
        private object[] OnEndUpLoadText(System.IAsyncResult result) {
            WebProject.TestService.TextEntity[] retVal = this.EndUpLoadText(result);
            return new object[] {
                    retVal};
        }
        
        private void OnUpLoadTextCompleted(object state) {
            if ((this.UpLoadTextCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.UpLoadTextCompleted(this, new UpLoadTextCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void UpLoadTextAsync() {
            this.UpLoadTextAsync(null);
        }
        
        public void UpLoadTextAsync(object userState) {
            if ((this.onBeginUpLoadTextDelegate == null)) {
                this.onBeginUpLoadTextDelegate = new BeginOperationDelegate(this.OnBeginUpLoadText);
            }
            if ((this.onEndUpLoadTextDelegate == null)) {
                this.onEndUpLoadTextDelegate = new EndOperationDelegate(this.OnEndUpLoadText);
            }
            if ((this.onUpLoadTextCompletedDelegate == null)) {
                this.onUpLoadTextCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnUpLoadTextCompleted);
            }
            base.InvokeAsync(this.onBeginUpLoadTextDelegate, null, this.onEndUpLoadTextDelegate, this.onUpLoadTextCompletedDelegate, userState);
        }
        
        public WebProject.TestService.TextEntity[] GetText() {
            return base.Channel.GetText();
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public System.IAsyncResult BeginGetText(System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginGetText(callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public WebProject.TestService.TextEntity[] EndGetText(System.IAsyncResult result) {
            return base.Channel.EndGetText(result);
        }
        
        private System.IAsyncResult OnBeginGetText(object[] inValues, System.AsyncCallback callback, object asyncState) {
            return this.BeginGetText(callback, asyncState);
        }
        
        private object[] OnEndGetText(System.IAsyncResult result) {
            WebProject.TestService.TextEntity[] retVal = this.EndGetText(result);
            return new object[] {
                    retVal};
        }
        
        private void OnGetTextCompleted(object state) {
            if ((this.GetTextCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.GetTextCompleted(this, new GetTextCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void GetTextAsync() {
            this.GetTextAsync(null);
        }
        
        public void GetTextAsync(object userState) {
            if ((this.onBeginGetTextDelegate == null)) {
                this.onBeginGetTextDelegate = new BeginOperationDelegate(this.OnBeginGetText);
            }
            if ((this.onEndGetTextDelegate == null)) {
                this.onEndGetTextDelegate = new EndOperationDelegate(this.OnEndGetText);
            }
            if ((this.onGetTextCompletedDelegate == null)) {
                this.onGetTextCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnGetTextCompleted);
            }
            base.InvokeAsync(this.onBeginGetTextDelegate, null, this.onEndGetTextDelegate, this.onGetTextCompletedDelegate, userState);
        }
    }
}
