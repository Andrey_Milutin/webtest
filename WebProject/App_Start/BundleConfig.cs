﻿using System.Web.Optimization;

namespace WebProject
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles-js/startup-actions").Include(
                  "~/Scripts/startupActions.js"));

            bundles.Add(new ScriptBundle("~/bundles-js/common-libs").Include(
                  "~/Scripts/bootstrap/bootstrap.js",
                  "~/Scripts/jquery/jquery.js",
                  "~/Scripts/jquery-ui/jquery-ui.js",
                  "~/Scripts/moment/moment.js",
                  "~/Scripts/underscore/underscore.js"));

            bundles.Add(new ScriptBundle("~/bundles-js/angular").Include(
                  "~/Scripts/angular/angular.js",
                  "~/Scripts/angular-sanitize/angular-sanitize.js"));

            bundles.Add(new ScriptBundle("~/bundles-js/angular-ui").Include(
                  "~/Scripts/angular-bootstrap/ui-bootstrap-tpls.js",
                  "~/Scripts/angular-touch/angular-touch.js",
                  "~/Scripts/angular-ui-grid/ui-grid.js",
                  "~/Scripts/angular-animate/angular-animate.js",
                  "~/Scripts/angular-ui-uploader/uploader.js"));

            bundles.Add(new ScriptBundle("~/bundles-js/common-module").Include(
                    "~/AngularJS/Common/view.js",
                  "~/AngularJS/Components/fileUploader.js"));

            bundles.Add(new ScriptBundle("~/bundles-js/home-module").Include(
                  "~/AngularJS/Home/home.js",
                  "~/AngularJS/Home/homeCtrl.js"));

            bundles.Add(new ScriptBundle("~/bundles-js/app-module").Include(
                  "~/AngularJS/app.js",
                  "~/AngularJS/mainCtrl.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap/css").Include(
                  "~/Content/bootstrap/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/angular-ui-grid/ui-grid.css",
                            "~/Content/site.css"));
        }
    }
}
