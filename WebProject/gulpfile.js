﻿/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
var bower = require('gulp-bower');
var concat = require('gulp-concat');
var karma = require('gulp-karma');
var clean = require('gulp-clean');
var replace = require('gulp-replace');

gulp.task('default', function () {
    // place code for your default task here
});

gulp.task('bower', function () {
    return bower('./bower_components');
});

gulp.task('clean', function () {
    return gulp
		.src([
			'Scripts/angular',
			'Scripts/angular-bootstrap',
			'Scripts/angular-ui-uploader',
			'Scripts/bootstrap',
			'Scripts/underscore',
			'Scripts/moment',
			'Scripts/jquery',
			'Content/bootstrap'
		], {
		    read: false
		})
        .pipe(clean());
});


gulp.task('copyLibs', ['bower'], function () {

    // angular
    gulp
		.src([
			'bower_components/angular/angular.js',
			'bower_components/angular/angular.min.js',
			'bower_components/angular/angular.min.js.map'
		])
        .pipe(gulp.dest('Scripts/angular'));

    // angular-sanitize
    gulp
		.src([
			'bower_components/angular-sanitize/angular-sanitize.js',
			'bower_components/angular-sanitize/angular-sanitize.min.js',
			'bower_components/angular-sanitize/angular-sanitize.min.js.map'
		])
	.pipe(gulp.dest('Scripts/angular-sanitize'));

    // angular-ui-grid
    gulp
        .src([
            'bower_components/angular-ui-grid/ui-grid.js',
            'bower_components/angular-ui-grid/ui-grid.min.js'
        ])
        .pipe(gulp.dest('Scripts/angular-ui-grid'));

    gulp
        .src([
            'bower_components/angular-ui-grid/ui-grid.css',
            'bower_components/angular-ui-grid/ui-grid.min.css',
            'bower_components/angular-ui-grid/ui-grid.eot',
            'bower_components/angular-ui-grid/ui-grid.svg',
            'bower_components/angular-ui-grid/ui-grid.ttf',
            'bower_components/angular-ui-grid/ui-grid.woff'
        ])
        .pipe(gulp.dest('Content/angular-ui-grid'));

    // angular-animate
    gulp
		.src([
			'bower_components/angular-animate/angular-animate.js',
			'bower_components/angular-animate/angular-animate.min.js',
			'bower_components/angular-animate/angular-animate.min.js.map'
		])
	.pipe(gulp.dest('Scripts/angular-animate'));

    // angular-touch
    gulp
		.src([
			'bower_components/angular-touch/angular-touch.js',
			'bower_components/angular-touch/angular-touch.min.js',
			'bower_components/angular-touch/angular-touch.min.js.map'
		])
	.pipe(gulp.dest('Scripts/angular-touch'));

    // jquery
    gulp
		.src(['bower_components/jquery/dist/*'])
        .pipe(gulp.dest('Scripts/jquery'));

    // jquery-ui
    gulp
		.src(['bower_components/jquery-ui/jquery-ui.js',
			'bower_components/jquery-ui/jquery-ui.min.js'
		])
		.pipe(gulp.dest('Scripts/jquery-ui'));

    // underscore
    gulp
		.src([
			'bower_components/underscore/underscore.js',
			'bower_components/underscore/underscore-min.js',
			'bower_components/underscore/underscore-min.map'
		])
		.pipe(gulp.dest('Scripts/underscore'));

    // moment
    gulp
		.src(['bower_components/moment/moment.js'])
        .pipe(gulp.dest('Scripts/moment'));

    // angular-ui-bootstrap
    gulp
		.src([
			'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
			'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
			'bower_components/angular-bootstrap/ui-bootstrap.js',
			'bower_components/angular-bootstrap/ui-bootstrap.min.js'
		])
		.pipe(gulp.dest('Scripts/angular-bootstrap'));

    // angular-ui-uploader
    gulp
		.src(['bower_components/angular-ui-uploader/dist/*'])
		.pipe(gulp.dest('Scripts/angular-ui-uploader'));

    // bootstrap
    gulp
		.src([
			'bower_components/bootstrap/dist/js/bootstrap.js',
			'bower_components/bootstrap/dist/js/bootstrap.min.js'
		])
		.pipe(gulp.dest('Scripts/bootstrap'));

    gulp
		.src(['bower_components/bootstrap/dist/css/*'])
        .pipe(gulp.dest('Content/bootstrap'));

    gulp
		.src(['bower_components/bootstrap/fonts/*'])
        .pipe(gulp.dest('Content/fonts'));

});